# KubPi : déploiement d'un cluster k8s sur raspberry
Création du cluster
```bash
ansible-playbook -i hosts.yml -u pi -k -b install-kub.yml
```

Reset all
```bash
ansible-playbook -i hosts.yml -u pi -k -b reset-all.yml
```

# Naming convention
## Hostname (System) + associated management address  
Values are fake
```yaml
pi1.rou.ras.baptiste-dauphin.com: 10.10.10.1
pi2.rou.ras.baptiste-dauphin.com: 10.10.10.2
pi3.rou.ras.baptiste-dauphin.com: 10.10.10.3
```

[A tuto](https://blog.serverdensity.com/picking-server-hostnames/)